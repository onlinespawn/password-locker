function hidePrompt(){
    document.getElementById("prompt").style.display="none";
}
function myPrompt(question,holder){
    document.getElementById("questionAnswer").value="";
    document.getElementById("questionAnswer").placeholder="";
    if(holder==undefined){
        
    }else{
        document.getElementById("questionAnswer").placeholder=holder;
    }
    document.getElementById("prompt").style.display="block";
    document.getElementById("question").innerHTML=question;
    document.getElementById("questionAnswer").focus();
}
function promptReturn(){
    hidePrompt();
    return decode(document.getElementById("questionAnswer").value);
}