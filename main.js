const electron = require('electron');
const { app, BrowserWindow } = require('electron');
let window;

function createWindow () {
  // Create the browser window.
  window = new BrowserWindow({ width: 800, height: 600, icon:  __dirname + '/folderLocker.svg.png' });

  // and load the index.html of the app.
  window.loadFile('index.html');
}
app.on('ready', createWindow);
app.on('window-all-closed', () => {
    app.quit();
});