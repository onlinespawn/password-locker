var decode = require('unescape');
function resize(){
    let coords = document.getElementById("options").getBoundingClientRect();
    let elHeight=coords.bottom;
    let height= window.innerHeight-elHeight;
    document.getElementById("accountHolder").style.height=height+"px";
    document.getElementById("detailsHolder").style.height=height+"px";
}
function nameLoader(){
    document.getElementById("accountHolder").innerHTML="";
    let folder=localFS.getFolderContents("./notebooks/"+localStorage.getItem('load'));
    for(i=0;i<folder.files.length;i++){
     document.getElementById("accountHolder").innerHTML+='<div class="accountName" onclick="loadAc(event);delLaunch(event);">'+folder.files[i]+'</div>';
    }
}
function openPrompt(){
    myPrompt("Account Name:","Google, Facebook, Github etc...");
}
function createAccount(){
    let name=promptReturn();
    if(name !== ""){
    let obj=new Object();
    obj.name="";
    obj.username="";
    obj.password="";
    obj.website="";
    obj.notes="";
    obj=JSON.stringify(obj);
    localFS.createFile("./notebooks/"+localStorage.getItem("load"),name,obj);
    nameLoader();
    }else{}
}
function loadAc(event){
    //alert(event.target.innerHTML.replace(/&amp;/g, '&'));
    let info= localFS.getFileContents("./notebooks/"+localStorage.getItem("load"),decode(event.target.innerHTML));
    if(info!==undefined){
    info=JSON.parse(info);
    document.getElementById("acNam").innerHTML=event.target.innerHTML;
    document.getElementById("name").value=info.name;
    document.getElementById("username").value=info.username;
    document.getElementById("password").value=info.password;
    document.getElementById("website").value=info.website;
    document.getElementById("notes").innerHTML=info.notes;
    }else{}
}
function updateState(){
    if(document.getElementById("acNam").innerHTML !== ""){
    //console.log("update");
    let obj=new Object();
    obj.name=document.getElementById("name").value;
    obj.username=document.getElementById("username").value;
    obj.password=document.getElementById("password").value;
    obj.website=document.getElementById("website").value;
    obj.notes=document.getElementById("notes").innerHTML;
    obj=JSON.stringify(obj);
    localFS.createFile("./notebooks/"+localStorage.getItem("load"),decode(document.getElementById("acNam").innerHTML),obj,true);
    }else{}
}
function delMode(event){
    if(event.target.id=="delete"){
        event.target.removeAttribute("id");
    }else{
        event.target.id="delete";
    }
}
let el;
function delLaunch(event){
    if(document.getElementById("delete")!==null){
    el=event.target;
    console.log(el.innerHTML);
    confirmLaunch("Are You Sure You Want To Delete "+decode(el.innerHTML)+"?");
    }else{}
}
function eliminate(){
    console.log("Eliminate "+el.innerHTML+" on "+"./notebooks/"+localStorage.getItem("load"),el.innerHTML);
    localFS.deleteFile("./notebooks/"+localStorage.getItem("load"),el.innerHTML);
    nameLoader();
    confirmReturn();
}
function cleanPage(){
    document.getElementById("acNam").innerHTML="";
    document.getElementById("name").innerHTML="";
    document.getElementById("username").innerHTML="";
    document.getElementById("password").innerHTML="";
    document.getElementById("website").innerHTML="";
}
function showHide(){
    //console.log(document.getElementById("password").style.fontFamily);
    if(document.getElementById("password").getAttribute("type")=="password"){
        document.getElementById("password").type="";
        document.getElementById("show-hide").src="eye.svg";
    }else{
        document.getElementById("password").type="password";
        document.getElementById("show-hide").src="eye-off.svg";
    }
}
const { clipboard } = require('electron');
function copyIt(){
    var copyText = document.getElementById("password").value;
    clipboard.writeText(copyText);
    document.getElementById("notification").style.display="block";
    setTimeout(function(){
        document.getElementById("notification").style.display="none";
    }, 700);
}