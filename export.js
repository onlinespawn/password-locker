var fs = require('fs'); // Load the File System to execute our common tasks (CRUD)
const {dialog} = require('electron').remote;

function exportPasswords(){
    let allData={
      name:[],
      stuff:[]
    };
    let content;
    for (var i = 0; i < localStorage.length; i++){
    allData.name.push(localStorage.key(i));
    allData.stuff.push(localStorage.getItem(localStorage.key(i)));
}
content=JSON.stringify(allData);
// You can obviously give a direct path without use the dialog (C:/Program Files/path/myfileexample.txt)
dialog.showSaveDialog({ properties: ['openFile'],filters: [{ name: 'Custom File Type', extensions: ['passwordLocker'] }] },(fileName) => {
    if (fileName === undefined){
        console.log("You didn't save the file");
        return;
    }
    if(fileName.substring((fileName.length-15),fileName.length)==".passwordLocker"){
        
    }else{
        fileName=fileName+".passwordLocker";
    }
    // fileName is a string that contains the path and filename created in the save file dialog.  
    fs.writeFile(fileName, content, (err) => {
        if(err){
            console.log("An error ocurred creating the file "+ err.message);
        }  
        console.log("The file has been succesfully saved");
    });
}); 
}