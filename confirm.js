let confAnswer=false;
function hideConfirm(){
    document.getElementById("confirm").style.display="none";
}
function confirmLaunch(question){
    confAnswer=false;
    document.getElementById("confirmQuestion").innerHTML=question;
    document.getElementById("confirm").style.display="block";
}
function real(){
    confAnswer=true;
    hideConfirm();
}
function confirmReturn(){
    hideConfirm();
    return confAnswer;
}