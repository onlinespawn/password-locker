var decode = require('unescape');
function openPrompt(){
    myPrompt("Notebook Name:","Enter notebook name");
}
function createNoteBook(){
    let noteName=promptReturn();
    if(noteName!==""){
    localFS.createFolder("./notebooks/"+noteName);
    refresh();
    }else{
        hidePrompt();
    }
}
function saveToLoadLater(event){
    localStorage.setItem("load",decode(event.target.innerHTML));
    window.location="notebook/loader.html";
}
function refresh(){
    document.getElementById("container").innerHTML="";
    localFS.createFolder("./notebooks");
    let noteBooks=localFS.getFolderContents("./notebooks");
    //console.log(noteBooks.folders);
    for(i=0;i<noteBooks.folders.length;i++){
    document.getElementById("container").innerHTML+='<div id="'+noteBooks.folders[i]+'" class="item" onclick="saveToLoadLater(event);">'+noteBooks.folders[i]+'</div><span id="'+noteBooks.folders[i]+'Delete" onclick="delLaunch(event);" class="delete">X</span>';
    }
}
let el="";
function delLaunch(event){
    el=event.target;
    confirmLaunch("Are You Sure You Want To Delete "+decode(document.getElementById(el.id.substring(0,(el.id.length-6))).innerHTML));
}
function deleteElement(){
    let justInCase=confirmReturn();
    if(justInCase){
    el.remove();
    localFS.deleteFolder("./notebooks/"+decode(document.getElementById(el.id.substring(0,(el.id.length-6))).innerHTML));
    document.getElementById(el.id.substring(0,(el.id.length-6))).remove();
    }else{}
}